I have poor near vision, and am still getting used to drawing with a tablet.
I made these oversized pen-themed cursors in an attempt to improve my
precision.

I have provided the original Krita document used to make each cursor. My
process to create each cursor package was as follows:

1. Create the cursor as a 128x128 image in Krita, mostly with the pixel art
brush, fuzzy select, and fill tool.
2. Export the image in XPM format.
3. Import the image into GIMP.
4. Export the image into XMC format. Set hotspot to 127x127 for bottom-right
   corner.

Alternatively, the xcursorgen tool may theoretically be used in step 3, if
the image is exported as a PNG in step 2. Personally, I wasn't able to
figure out how to preserve transparency with this method.
